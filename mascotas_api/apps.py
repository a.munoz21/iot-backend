from django.apps import AppConfig


class MascotasApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mascotas_api'
