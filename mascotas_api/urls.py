from django.urls.resolvers import URLPattern
from . import views
from django.conf.urls import url, include 
from django.urls import path
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'clients', views.ClientViewSet)
router.register(r'pets', views.PetViewSet)
router.register(r'lectures', views.LecturesViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
urlpatterns += [
    path('login/', views.login)
]