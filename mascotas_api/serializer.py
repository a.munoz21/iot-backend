from .models import Client,Pet,Lectures
from rest_framework import serializers
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('first_name', 'last_name','rut','phone_number')

class PetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pet
        fields = ('name', 'master')

class LectureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lectures
        fields = ('temperature', 'location','pet')

