from django.shortcuts import render
from .models import Client,Pet,Lectures
from rest_framework import viewsets
from .serializer import ClientSerializer,PetSerializer,LectureSerializer
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.hashers import check_password

@api_view(['POST'])
def login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')

    try: 
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return Response("Usuario inválido")

    pwd_valid = check_password(password,user.password)

    if not pwd_valid:
        return Response('Contraseña inválida')

    token, created = Token.objects.get_or_create(user=user)
    return Response(token.key)

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all().order_by('created_date')
    serializer_class = ClientSerializer
    permission_classes = [IsAuthenticated]

class PetViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all().order_by('created_date')
    serializer_class = PetSerializer
    permission_classes = [IsAuthenticated]

class LecturesViewSet(viewsets.ModelViewSet):
    queryset = Lectures.objects.all().order_by('created_date')
    serializer_class = LectureSerializer
    permission_classes = [IsAuthenticated]
