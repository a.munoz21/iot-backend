from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    rut = models.IntegerField()
    phone_number = models.IntegerField()
    created_date = models.DateTimeField(
            default=timezone.now)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __str__(self):
        return self.first_name

class Pet(models.Model):
    name = models.CharField(max_length=30)
    master = models.ForeignKey(Client,on_delete=models.CASCADE)
    created_date = models.DateTimeField(
            default=timezone.now)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name

class Lectures(models.Model):
    temperature = models.FloatField()
    location = models.CharField(max_length=100)
    pet = models.ForeignKey(Pet,on_delete=models.CASCADE)
    created_date = models.DateTimeField(
            default=timezone.now)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __str__(self):
        return self.temperature